function Heading() {
    return (
    <>
      <div class="headingDiv">
        <h1><b>All Articles</b></h1>
        <h3>Collection of best articles on Startups</h3>
        </div>
        <div class = "box">
        <Cards/>
        <Cards/>
        <Cards/>
        </div>
    </>
    );
  }

function Cards(){
   return (
   <div class= "cardsDiv">
        <div class="photo">
        <img class="images" src="https://images.unsplash.com/photo-1490358930084-2d26f21dc211?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1332&q=80" width ="100%" height = "225px"/>
        <div class = "dateTag">27 MAR</div>
        <div class = "photoTag">PHOTOS</div>
        </div>
        <p class="titleLine">City Lights in New York</p>
        <h4>The city that never sleep.</h4>
        <p class="para">
            New York, the largest city in the US., is an
            architectural marvel with plenty of historic
            monuments, magnificent buildings and
            countiess dazzling skyscrapers.
        </p>
        <div class="commentAndAgo" >&#9675; mins ago &nbsp; <img src="./images/comments-solid.svg"  width = "20px"  height = "12px" /> 39 comments</div>
    </div>)

}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Heading/>);